defmodule Nhangman.MixProject do
  use Mix.Project

  def project do
    [
      app: :nhangman,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Nhangman.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug_cowboy, "~> 2.0"},
      {:uuid, "~> 1.1"},
      {:poison, "~> 4.0"},
      {:dictionary, github: "rawandrew/dictionary"}
    ]
  end
end

defmodule GameRouter do
  use Plug.Router

  plug :match
  plug Plug.Parsers, parsers: [:json], pass: ["application/json"], json_decoder: Poison
  plug :dispatch

  def start_link do
    Plug.Adapters.Cowboy2.http(GameRouter, [])
  end

  post "/games" do
    id = UUID.uuid1()
    {:ok, _} = Game.start_link(String.to_atom(id))
    conn
    |> put_resp_header("Location", "/games/#{id}")
    |> send_resp(201, "Your game has been created")
  end

  get "/games/:id" do
    if Process.whereis(String.to_atom(id)) != nil do
      {formatted, _status, _turns} = Game.get_feedback(String.to_atom(id))
      send_resp(conn, 200, Poison.encode!(%{
        msg: "This is your game's state", 
        formatted: formatted
      }))
    else
      send_resp(conn, 404, "Game URL is not know")
    end
  end

  post "/games/:id/guesses" do
    IO.inspect conn.body_params
    send_resp(conn, 201, "Nice move")
  end

  match _ do
    send_resp(conn, 404, "Oops")
  end
end
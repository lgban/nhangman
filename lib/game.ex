defmodule Game do
    use GenServer

    def start_link(name) do
      word = Dictionary.random_word |> String.trim
      GenServer.start_link(__MODULE__, word, name: name)
    end
  
    def init(secret_word) do
      {:ok, {secret_word, "", "", 9}}
    end

    def get_feedback(pid) do
      GenServer.call(pid, :get_feedback)
    end

    def submit_guess(pid, guess) do
      GenServer.cast(pid, {:submit_guess, guess})
    end
  
    def handle_call(:get_feedback, _from, {_, _, _, turns_remaining} = game) do
      state =
        cond do
          String.contains?(Hangman.format_feedback(game), "-") and turns_remaining > 0 ->
            :playing
          String.contains?(Hangman.format_feedback(game), "-") and turns_remaining == 0 -> 
            :lose
          true -> :win
        end
      {:reply, {Hangman.format_feedback(game), state, turns_remaining}, game}
    end
  
    def handle_cast({:submit_guess, guess}, game) do
      {:noreply, Hangman.score_guess(game, guess)}
    end
end